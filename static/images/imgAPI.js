const imgAPI = {
  avatar: [
    '/images/avatars/PA.png',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy.svg',
    '/images/avatars/pp_boy.svg'
    // '/images/avatars/pp_boy.svg',
    // '/images/avatars/pp_boy.svg',
    // '/images/avatars/pp_boy.svg',
    // '/images/avatars/pp_boy.svg'
  ],
  photo: [
    'https://via.placeholder.com/675x900/e1ad92/fff',
    'https://via.placeholder.com/967x725/ea6d6d/fff',
    'https://via.placeholder.com/1280x849/ea6db7/fff',
    'https://via.placeholder.com/967x725/bb6dea/fff',
    'https://via.placeholder.com/1048x701/6d6fea/fff',
    'https://via.placeholder.com/1050x700/6dc0ea/fff',
    'https://via.placeholder.com/970x725/6deaa6/fff',
    'https://via.placeholder.com/1051x700/b8de27/fff',
    'https://via.placeholder.com/1051x701/de9f27/fff',
    'https://via.placeholder.com/1050x700/ef4545/fff',
    'https://via.placeholder.com/1050x700/ffc4c4/757575',
    'https://via.placeholder.com/640x447/fdffc4/757575',
    'https://via.placeholder.com/1280x851/c4ffd7/757575',
    'https://via.placeholder.com/640x425/c4cdff/757575'
  ],
  crypto: [
    '/images/crypto/hexa-orange.png',
    '/images/crypto/hexa-green.png',
    '/images/crypto/hexa-orange.png',
    '/images/crypto/hexa-green.png',
    '/images/crypto/1280x766-01.png',
    '/images/crypto/1280x766-01.png',
    '/images/crypto/crypto_laptop2.png'
    // /images/crypto/1280x766-01.png
  ]
}

export default imgAPI
