const link = {
  crypto: {
    home: '/',
    contact: '/contact',
    login: '/login',
    register: '/register',
    blanks: '/blank-page',
    facebook: 'https://www.facebook.com/groups/amaziefintech/',
    Instagram: 'https://www.instagram.com/amaziefintech/',
    LinkInL: 'https://www.linkedin.com/company/amaziefintech'
  }
}

export default link
