const brand = {
  crypto: {
    name: 'Amazie Fintech',
    desc: 'Veluxi Cryptocurrency - Vue Single Landing Page Template',
    prefix: 'veluxi',
    footerText: 'Veluxi Theme 2020',
    logoText: 'Veluxi Coinz',
    projectName: 'Amazie Fintech',
    url: 'veluxi.ux-maestro.com/cryptocurrency',
    // img: '/static/images/crypto-logo.png',
    img: '~/static/images/logo-01.png',
    notifMsg:
      'Donec sit amet nulla sed arcu pulvinar ultricies commodo id ligula.'
  }
}

export default brand
