import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _69ace953 = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _3f0be596 = () => interopDefault(import('../pages/admin/index/index.vue' /* webpackChunkName: "pages/admin/index/index" */))
const _6b2464db = () => interopDefault(import('../pages/admin/index/dashboard/index.vue' /* webpackChunkName: "pages/admin/index/dashboard/index" */))
const _046d1678 = () => interopDefault(import('../pages/admin/index/member/index1.vue' /* webpackChunkName: "pages/admin/index/member/index1" */))
const _56e4f737 = () => interopDefault(import('../pages/blank-page.vue' /* webpackChunkName: "pages/blank-page" */))
const _91eece3e = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _e6665954 = () => interopDefault(import('../pages/KYC.vue' /* webpackChunkName: "pages/KYC" */))
const _6b8f0dea = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _7ab54ab2 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _5daf625d = () => interopDefault(import('../pages/user/index.vue' /* webpackChunkName: "pages/user/index" */))
const _e6e421c0 = () => interopDefault(import('../pages/user/index/index.vue' /* webpackChunkName: "pages/user/index/index" */))
const _2149ae28 = () => interopDefault(import('../pages/user/index/brokerdashboard/index.vue' /* webpackChunkName: "pages/user/index/brokerdashboard/index" */))
const _e890ec36 = () => interopDefault(import('../pages/user/index/dashboard/index.vue' /* webpackChunkName: "pages/user/index/dashboard/index" */))
const _c1957e06 = () => interopDefault(import('../pages/user/index/information/index.vue' /* webpackChunkName: "pages/user/index/information/index" */))
const _bc52194c = () => interopDefault(import('../pages/user/index/invest/index.vue' /* webpackChunkName: "pages/user/index/invest/index" */))
const _a23e1b44 = () => interopDefault(import('../pages/user/index/wallet/index.vue' /* webpackChunkName: "pages/user/index/wallet/index" */))
const _eff19f00 = () => interopDefault(import('../pages/user/index/dashboard/index copy.vue' /* webpackChunkName: "pages/user/index/dashboard/index copy" */))
const _4105955a = () => interopDefault(import('../pages/user/index/invest/dividend/index.vue' /* webpackChunkName: "pages/user/index/invest/dividend/index" */))
const _63bb5714 = () => interopDefault(import('../pages/user/index/invest/selection.vue' /* webpackChunkName: "pages/user/index/invest/selection" */))
const _0f8dd39f = () => interopDefault(import('../pages/user/index/invest/shares/index.vue' /* webpackChunkName: "pages/user/index/invest/shares/index" */))
const _db1e5fa6 = () => interopDefault(import('../pages/user/index/wallet/creditcard.vue' /* webpackChunkName: "pages/user/index/wallet/creditcard" */))
const _0e3cd741 = () => interopDefault(import('../pages/user/index/wallet/ibanking.vue' /* webpackChunkName: "pages/user/index/wallet/ibanking" */))
const _42c0388f = () => interopDefault(import('../pages/user/index/wallet/transfer.vue' /* webpackChunkName: "pages/user/index/wallet/transfer" */))
const _23afe42e = () => interopDefault(import('../pages/user/index/wallet/withdraw.vue' /* webpackChunkName: "pages/user/index/wallet/withdraw" */))
const _2a8d2adc = () => interopDefault(import('../pages/user/index/invest/dividend/agreement.vue' /* webpackChunkName: "pages/user/index/invest/dividend/agreement" */))
const _446051ba = () => interopDefault(import('../pages/user/index/invest/dividend/contract.vue' /* webpackChunkName: "pages/user/index/invest/dividend/contract" */))
const _ba820352 = () => interopDefault(import('../pages/user/index/invest/shares/agreement.vue' /* webpackChunkName: "pages/user/index/invest/shares/agreement" */))
const _b5b29d56 = () => interopDefault(import('../pages/user/index/invest/shares/contract.vue' /* webpackChunkName: "pages/user/index/invest/shares/contract" */))
const _45ec3d5c = () => interopDefault(import('../pages/user/index/wallet/result/fail.vue' /* webpackChunkName: "pages/user/index/wallet/result/fail" */))
const _491c0696 = () => interopDefault(import('../pages/user/index/wallet/result/success.vue' /* webpackChunkName: "pages/user/index/wallet/result/success" */))
const _338b1b48 = () => interopDefault(import('../pages/verify.vue' /* webpackChunkName: "pages/verify" */))
const _5e67a6e4 = () => interopDefault(import('../pages/waiting.vue' /* webpackChunkName: "pages/waiting" */))
const _bc312a5a = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin",
    component: _69ace953,
    children: [{
      path: "",
      component: _3f0be596,
      name: "admin-index___en"
    }, {
      path: "dashboard",
      component: _6b2464db,
      name: "admin-index-dashboard___en"
    }, {
      path: "member/index1",
      component: _046d1678,
      name: "admin-index-member-index1___en"
    }]
  }, {
    path: "/blank-page",
    component: _56e4f737,
    name: "blank-page___en"
  }, {
    path: "/contact",
    component: _91eece3e,
    name: "contact___en"
  }, {
    path: "/KYC",
    component: _e6665954,
    name: "KYC___en"
  }, {
    path: "/login",
    component: _6b8f0dea,
    name: "login___en"
  }, {
    path: "/register",
    component: _7ab54ab2,
    name: "register___en"
  }, {
    path: "/user",
    component: _5daf625d,
    children: [{
      path: "",
      component: _e6e421c0,
      name: "user-index___en"
    }, {
      path: "brokerdashboard",
      component: _2149ae28,
      name: "user-index-brokerdashboard___en"
    }, {
      path: "dashboard",
      component: _e890ec36,
      name: "user-index-dashboard___en"
    }, {
      path: "information",
      component: _c1957e06,
      name: "user-index-information___en"
    }, {
      path: "invest",
      component: _bc52194c,
      name: "user-index-invest___en"
    }, {
      path: "wallet",
      component: _a23e1b44,
      name: "user-index-wallet___en"
    }, {
      path: "dashboard/index copy",
      component: _eff19f00,
      name: "user-index-dashboard-index copy___en"
    }, {
      path: "invest/dividend",
      component: _4105955a,
      name: "user-index-invest-dividend___en"
    }, {
      path: "invest/selection",
      component: _63bb5714,
      name: "user-index-invest-selection___en"
    }, {
      path: "invest/shares",
      component: _0f8dd39f,
      name: "user-index-invest-shares___en"
    }, {
      path: "wallet/creditcard",
      component: _db1e5fa6,
      name: "user-index-wallet-creditcard___en"
    }, {
      path: "wallet/ibanking",
      component: _0e3cd741,
      name: "user-index-wallet-ibanking___en"
    }, {
      path: "wallet/transfer",
      component: _42c0388f,
      name: "user-index-wallet-transfer___en"
    }, {
      path: "wallet/withdraw",
      component: _23afe42e,
      name: "user-index-wallet-withdraw___en"
    }, {
      path: "invest/dividend/agreement",
      component: _2a8d2adc,
      name: "user-index-invest-dividend-agreement___en"
    }, {
      path: "invest/dividend/contract",
      component: _446051ba,
      name: "user-index-invest-dividend-contract___en"
    }, {
      path: "invest/shares/agreement",
      component: _ba820352,
      name: "user-index-invest-shares-agreement___en"
    }, {
      path: "invest/shares/contract",
      component: _b5b29d56,
      name: "user-index-invest-shares-contract___en"
    }, {
      path: "wallet/result/fail",
      component: _45ec3d5c,
      name: "user-index-wallet-result-fail___en"
    }, {
      path: "wallet/result/success",
      component: _491c0696,
      name: "user-index-wallet-result-success___en"
    }]
  }, {
    path: "/verify",
    component: _338b1b48,
    name: "verify___en"
  }, {
    path: "/waiting",
    component: _5e67a6e4,
    name: "waiting___en"
  }, {
    path: "/",
    component: _bc312a5a,
    name: "index___en"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
