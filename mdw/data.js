const express = require('express')

const app = express()

app.use(express.json())

app.get('/cdc', function(req, res) {
  console.log(req.query)

  const request = require('request')

  let data = {
    amount: req.query.amount,
    token: req.query.omiseToken
  }

  request(
    {
      url: 'http://192.168.1.72:8004/api/check-credit-card',
      method: 'POST',
      json: data,
      headers: {
        Authorization: 'Bearer ' + req.query.auth
      }
    },
    (err, r, body) => {
      //   let resp = JSON.parse(body)
      let resp = body
      console.log(resp)
      //   console.log(resp.success)
      if (resp.success == 'success') {
        res.writeHead(302, {
          Location: `/user/wallet/result/success`
        })
      } else {
        res.writeHead(302, {
          Location: `/user/wallet/result/fail`
        })
      }
      res.end()
    }
  )
})

app.get('/ibk', function(req, res) {
  console.log(req.query)

  const request = require('request')

  let data = {
    amount: req.query.amount,
    source: req.query.omiseToken
  }

  request(
    {
      url: 'http://192.168.1.72:8004/api/check-internet-banking',
      method: 'POST',
      json: data,
      headers: {
        Authorization: 'Bearer ' + req.query.auth
      }
    },
    (err, r, body) => {
      //   let resp = JSON.parse(body)
      let resp = body
      // console.log(resp)
      console.log(resp.message.authorize_uri)

      res.writeHead(302, {
        Location: resp.message.authorize_uri
      })

      //   res.writeHead(302, {
      //     Location: `/user/wallet/success`
      //   })

      res.end()
    }
  )
})

module.exports = {
  path: '/mdw',
  handler: app
}
