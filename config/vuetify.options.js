import colors from 'vuetify/lib/util/colors'

let darkMode = false
if (typeof Storage !== 'undefined') { // eslint-disable-line
  darkMode = localStorage.getItem('luxiDarkMode') || false
}

/** !IMPORTANT
** If you change the palette bellow,
** don't forget to update /crypto-theme/components/GradientDeco/GradientDeco.vue on const palette as well.
** Make sure the const palette has same value as this const palette bellow
**/

const palette = {
  money: {
    primary: colors.green.base, // primary main
    primarylight: colors.green.lighten4, // primary light
    primarydark: colors.green.darken3, // primary dark
    secondary: colors.amber.darken2, // secondary main
    secondarylight: colors.amber.lighten4, // secondary light
    secondarydark: colors.amber.darken4, // secondary dark
    anchor: colors.green.base // link
  },

  black: {
    primary: colors.grey.darken4, // primary main
    primarylight: colors.grey.base, // primary light
    primarydark: colors.shades.black, // primary dark
    secondary: colors.grey.darken1, // secondary main
    secondarylight: colors.grey.lighten2, // secondary light
    secondarydark: colors.grey.darken3, // secondary dark
    anchor: colors.grey.base, // link
    registerOrange: '#FEA018', // register continue button
    orange: '#FB8C00', // overall orange
    green: '#2E8B03', // overall green
    lgreen: '#6DAC40', // overall light green
  },
  cloud: {
    primary: colors.lightBlue.base, // primary main
    primarylight: colors.lightBlue.lighten4, // primary light
    primarydark: colors.lightBlue.darken4, // primary dark
    secondary: colors.orange.base, // secondary main
    secondarylight: colors.orange.lighten4, // secondary light
    secondarydark: colors.orange.darken4, // secondary dark
    anchor: colors.lightBlue.base // link
  }
}

export const theme = { ...palette.black }

export default {
  rtl: false,
  theme: {
    // dark: darkMode === 'true',
    dark: darkMode === 'true', //เปลี่ยน theme
    themes: {
      light: {
        ...theme
      },
      dark: {
        ...theme
      }
    },
    options: {
      customProperties: true
    }
  }
}
